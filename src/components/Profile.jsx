import React, { Component } from 'react';
import {
  Person,
} from 'blockstack';


export default class Profile extends Component {
  constructor(props) {
  	super(props);

  	this.state = {
  	  person: {
  	  	name() {
          return 'Anonymous';
        },

    
  	  },
  	};
  }

  render() {
    const { handleSignOut, userSession } = this.props;
    const { person } = this.state;
    
    console.log(person.avatarUrl())
    return (
      !userSession.isSignInPending() ?
      <div className="panel-welcome" id="section-2">
      <nav className="navbar">
        <ul className="navbar-nav">
   
          <li className="pdt-5">
            <h5 > Dashboard</h5>
          </li>
        {/*
          <li className="pdt-5">
           <a href="/clients"> <h5 > Clients </h5> </a>
          </li>
          <li className="pdt-5">
          <a href="/projects"><h5 > Projects </h5> </a>
          </li>
       
          <li className="pdt-5">
          <a href="/payments"> <h5 > Payments </h5> </a>
          </li>
           */}
          <li className="logout"> 
          <button
            className="btn btn-primary btn-md logout-button"
            id="signout-button"
            onClick={ handleSignOut.bind(this) }>
                   Logout
          </button>
         </li>
      </ul>
      </nav>
      <div>
      <p className="nav-adjuster">
        <div className="profile-url">
          <img src={person.avatarUrl() ? person.avatarUrl()  : null } alt="" className="rounded-profile" />
          <h1 className="profile-name" ><b><span id="heading-name">{ person.name() ? person.name() : 'Nameless Person' }</span></b></h1>
        </div>
     </p> 

        <div className="containers-dashboard">
          <h1 className="header-dashboard" > Clients </h1>
          <button className="btn btn-primary btn-md new-button"> Create New </button>  
        </div>

        <div className="containers-dashboard">
          <h1 className="header-dashboard"> Projects </h1>
          <button className="btn btn-primary btn-md new-button"> Create New </button>  
        </div>


     </div>


      </div> : null
    );
  }

  componentWillMount() {
    const { userSession } = this.props;
    this.setState({
      person: new Person(userSession.loadUserData().profile),

    });
  }
}
