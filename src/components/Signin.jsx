import React, { Component } from 'react';

export default class Signin extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    const { handleSignIn } = this.props;

    return (
      <div className="container">
        <h1 className="">Welcome to ClientSyd!</h1>
        <h5 className="">The ultimate tool for project transparency.</h5>
        <p className="">
          <button
            className="btn btn-primary btn-lg"
            onClick={ handleSignIn.bind(this) }
          >
            Sign In with Blockstack
          </button>
        </p>
      </div>
    );
  }
}
